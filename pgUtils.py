#!/usr/bin/env python
#
#  CartoDB PostgreSQL Checks - Common Functions
#  ============================================
#
##

import string
import random
import configparser
import psycopg2


def randNum(length):
    '''
    Return random integer

    Parameters:
    length      - string size
    '''
    return random.randrange(length)


def randString(length):
    '''
    Return random string

    Parameters:
    length      - string size
    '''
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


def readCfg(section, path='config.ini'):
    '''
    Reads database config section from file

    Parameters:
    section     - section to read from file
    path        - default to 'config.ini'
    '''

    # Create cfg parser
    parser = configparser.ConfigParser()

    # Read cfg file
    parser.read(path)

    config = {}

    # Parse the specified cfg section
    if parser.has_section(section):
        for k, v in parser[section].items():
            config[k] = v
    else:
        raise Exception(
            "Section '{}' not found in config file!"
            .format(section)
        )

    return config


def dbConnect(database=None):
    '''
    Connects to database

    Parameters:
    database        - database to connect
    '''

    # Read DB connection config
    dbCfg = readCfg('server')

    # Change default DB if specified
    if database:
        dbCfg['dbname'] = database

    # Connect to DB
    conn = psycopg2.connect(**dbCfg)
    conn.autocommit = True

    return conn


def dbQuery(conn, query, vars=None):
    '''
    Executes query to database

    Parameters:
    query       - query to execute as string
    '''

    # Initialize DB cursor
    dbcur = conn.cursor()

    # Execute the query
    dbcur.execute(query, vars)

    # Fetch data if any
    try:
        queryOut = dbcur.fetchall()
    except:
        pass

    # Close DB cursor
    dbcur.close()

    try:
        return queryOut
    except:
        pass
