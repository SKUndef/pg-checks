#!/usr/bin/env python
#
#  CartoDB PostgreSQL Checks - Database server statistics
#  ======================================================
#
##

import json
import argparse
import pgUtils


# FUNCTIONS

def initVars():
    '''
    Initializes input variables
    '''

    # Initialize arguments parser
    argParser = argparse.ArgumentParser(
        description='''Retrieves PostgreSQL server statistics.'''
    )

    # Parse args
    argParser.parse_args()


def parseRows():
    '''
    Retrieves average rows per database and total rows in server
    '''

    # Initialize total rows count
    totRowsCount = 0

    # Get databases list
    dbList = pgUtils.dbQuery(
                dbconn,
                'SELECT datname FROM pg_database WHERE datistemplate=false;'
             )

    for dbRow in dbList:

        # Temporary connection to database
        tmpDbConn = pgUtils.dbConnect(dbRow[0])

        # Initialize database row count
        dbRowsCount = 0

        # Retrieve tables list
        tbList = pgUtils.dbQuery(
                    tmpDbConn,
                    '''SELECT tablename FROM pg_tables
                       WHERE schemaname!='pg_catalog'
                       AND schemaname!='information_schema';'''
                 )

        for tbRow in tbList:

            # Add table rows count to database count
            dbRowsCount += pgUtils.dbQuery(
                                tmpDbConn,
                                '''SELECT count(*) FROM {};'''.format(tbRow[0])
                           )[0][0]

        # Close temporary connection
        tmpDbConn.close()

        # Add database rows count to total
        totRowsCount += dbRowsCount

    # Calculate average rows per database
    avgRowsCount = int(totRowsCount/len(dbList))

    # Return formatted output
    return json.dumps({
        'avgRows': avgRowsCount,
        'totRows': totRowsCount
    })


# MAIN

initVars()

# Initialize master connection to DB
dbconn = pgUtils.dbConnect()

# Analyze DB server rows
result = parseRows()

# Close master connection to DB
dbconn.close()

# Print output
print(result)
