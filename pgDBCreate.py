#!/usr/bin/env python
#
#  CartoDB PostgreSQL Checks - Database and Tables random creation
#  ===============================================================
#
##

import json
import argparse
import pgUtils


# FUNCTIONS

def initVars():
    '''
    Initializes input variables
    '''

    global dbNum
    global tbNum

    # Retrieve configuration
    conf = pgUtils.readCfg('pgDBCreate.js')

    # Initialize arguments parser
    argParser = argparse.ArgumentParser(
        description='''Creates PostgreSQL databases and tables with random records.'''
    )

    # Add expected arguments to args parser
    argParser.add_argument(
        '-d', '--databases',
        type=int,
        help="Number of databases to create",
        default=conf['db_num']
    )
    argParser.add_argument(
        '-t', '--tables',
        type=int,
        help="Number of tables to create",
        default=conf['table_num']
    )

    # Parse args
    args = argParser.parse_args()

    dbNum = args.databases
    tbNum = args.tables


def createDBTables(dbNum, tbNum):
    '''
    Creates databases and tables randomly

    Parameters:
    dbNum       - number of databases to create
    tbNum       - number of tables to create
    '''

    for _ in range(dbNum):

        # Randomize database name
        dbName = pgUtils.randString(10)

        # Create database
        pgUtils.dbQuery(dbconn, '''CREATE DATABASE {};'''.format(dbName))

        # Connect to new database
        tmpDbConn = pgUtils.dbConnect(dbName)

        for _ in range(tbNum):

            # Randomize table name
            tbName = pgUtils.randString(10)

            # Create table
            pgUtils.dbQuery(
                tmpDbConn,
                '''CREATE TABLE {} (id serial PRIMARY KEY, num integer, data varchar);'''
                .format(tbName)
            )

            # Insert random records in new table
            for _ in range(pgUtils.randNum(20)):
                pgUtils.dbQuery(
                    tmpDbConn,
                    '''INSERT INTO {} (num, data) VALUES (%s, %s);'''.format(tbName),
                    (pgUtils.randNum(10000), pgUtils.randString(25))
                )

        # Close connection to new database
        tmpDbConn.close()

    return json.dumps({'status': 'OK'})


# MAIN

initVars()

# Initialize master connection to DB
dbconn = pgUtils.dbConnect()

# Create databases and tables
result = createDBTables(dbNum, tbNum)

# Close master connection to DB
dbconn.close()

# Print output
print(result)
