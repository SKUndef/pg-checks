CartoDB PostgreSQL Checks
============================

This is a collection of scripts for PostgreSQL test and maintenance.

---

## Installation

1. Install required libraries: configparser, argparser, psycopg2
2. Clone the repository
3. Modify [config.ini](config.ini) with credentials for database server connection and other thresholds

**Note**: it's suggested to deploy scripts on the same database server, and set `host` parameter in config file to `localhost` or `127.0.0.1`. This is required for disk space usage check to work (integration with remote hosts through SSH/other can come in future versions).

## Description

The scripts collection is made of following files:

**config.ini**

This file contains configuration with following sections:

- [server]
    + host: IP address of database server
    + port: Host port of database server
    + dbname: default database for first connection (usually `postgres`)
    + user: database server username
    + password: database server password
- [pgDBCreate.js]
    + db_num: number of databases to create
    + table_num: number of tables to create for each database
- [pgDBCheck.py]
    + max_db_num: max number of databases allowed
    + max_disk_perc: max percentage of disk usage allowed
    + max_conn_perc: max percentage of connections allowed

**pgUtils.py**

This file contains functions for database connection, config parsing and other small utilities. This module is imported from other main scripts.

**pgDBCreate.py**

This scripts injects randomly databases and tables, and fills them with random records. Options for database and table numbers to create if provided will override config settings.

> pgDBCreate.py -h

```
usage: pgDBCreate.py [-h] [-d DATABASES] [-t TABLES]

Creates PostgreSQL databases and tables with random records.

optional arguments:
  -h, --help            show this help message and exit
  -d DATABASES, --databases DATABASES
                        Number of databases to create
  -t TABLES, --tables TABLES
                        Number of tables to create
```

**pgDBCheck.py**

This script executes basic checks on PostgreSQL health. Mode can be specified to execute a particular check, useful if integrated in monitoring systems to retrieve one particular metric. If omitted, it executes all checks.
If any of the checks fails when tested against thresholds, a report file is produced in `reports` directory.

> pgDBCheck.py -h

```
usage: pgDBCheck.py [-h] [--mode MODE]

Checks PostgreSQL server healthy.

optional arguments:
  -h, --help   show this help message and exit
  --mode MODE  Type of check to execute. 'dbnum' - Number of databases, 'disk'
               - Percentage of used disk space, 'conn' - Number of
               connections, 'all' - Executes all checks.
```

**pgServerStats.py**

This script retrieves statistics on PostgreSQL server about number of rows contained. It outputs average number of rows per database and total number of rows in whole server.

> pgServerStats.py -h

```
usage: pgServerStats.py [-h]

Retrieves PostgreSQL server statistics.

optional arguments:
  -h, --help  show this help message and exit
```

## Output

Every output is formatted as **JSON**. This should be easily compatible with any (metrics) system that needs to parse it. Different output formats could be added in future.