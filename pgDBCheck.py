#!/usr/bin/env python
#
#  CartoDB PostgreSQL Checks - Database server checks
#  ==================================================
#
##

import os
import time
import json
import subprocess
import argparse
import pgUtils


# FUNCTIONS

def initVars():
    '''
    Initializes input variables
    '''

    global mode
    global maxDbNum
    global maxDiskPerc
    global maxConnPerc
    global reportFile

    # Retrieve configuration
    conf = pgUtils.readCfg('pgDBCheck.py')

    # Initialize arguments parser
    argParser = argparse.ArgumentParser(
        description='''Checks PostgreSQL server healthy.'''
    )

    # Add expected arguments to args parser
    argParser.add_argument(
        '--mode',
        type=str,
        help="Type of check to execute. "
             "'dbnum' - Number of databases, "
             "'disk' - Percentage of used disk space, "
             "'conn' - Number of connections, "
             "'all' - Executes all checks.",
        choices=['all', 'dbnum', 'disk', 'conn'],
        default='all'
    )

    # Parse args
    args = argParser.parse_args()

    mode = args.mode
    maxDbNum = int(conf['max_db_num'])
    maxDiskPerc = int(conf['max_disk_perc'])
    maxConnPerc = int(conf['max_conn_perc'])
    reportFile = 'reports/{}_{}_fails.log'.format(
                    time.strftime('%Y%m%d_%H%M'),
                    mode
                  )


def checkDBNum():
    '''
    Checks number of created databases
    '''

    # Get databases number
    dbNum = pgUtils.dbQuery(
                dbconn,
                'SELECT count(*) FROM pg_database WHERE datistemplate=false;'
            )[0][0]

    # If number of databases is over threshold, report it
    if dbNum > maxDbNum:

        status = False

        if not os.path.exists('reports'):
            os.mkdir('reports')

        f = open(reportFile, 'a')
        f.write(
            'Number of databases over threshold: {} (max {})\n'
            .format(dbNum, maxDbNum)
        )
        f.close()
    else:
        status = True

    # Return formatted output
    return json.dumps({'dbNum': dbNum, 'passed': status})


def checkDBDisk():
    '''
    Checks percentage of PostgreSQL used disk space

    Notes:
    To execute this check is necessary to run the script
    on the server where PostgreSQL is running.
    This means that 'host' in config file must be
    equal to 'localhost' or '127.0.0.1'.
    '''

    # Check if script is running on localhost
    if pgUtils.readCfg('server')['host'] not in ('localhost', '127.0.0.1'):
        raise Exception('Cannot execute the check on remote systems!')

    # Get PostgreSQL data directory
    pgDataDir = pgUtils.dbQuery(dbconn, 'SHOW data_directory;')[0][0]

    # Get PostgreSQL disk usage
    pgDataSize = subprocess.check_output(["du", "-sb", pgDataDir]).split()[0]

    # Get filesystem stats
    fsStats = os.statvfs(pgDataDir)

    # Get total filesystem size (blocks)
    fsTotSize = fsStats.f_blocks * fsStats.f_bsize

    # Calculate percentage of used space
    pgUsedPerc = (int(pgDataSize)*100.0)/fsTotSize

    if pgUsedPerc > maxDiskPerc:

        status = False

        if not os.path.exists('reports'):
            os.mkdir('reports')

        f = open(reportFile, 'a')
        f.write(
            'PostgreSQL used space over threshold: {}% (max {}%)\n'
            .format(pgUsedPerc, maxDiskPerc)
        )
        f.close()
    else:
        status = True

    # Return formatted output
    return json.dumps({
        'pgDiskPerc': "{0:.1f}".format(pgUsedPerc),
        'passed': status
    })


def checkDBConn():
    '''
    Checks number of database active connections
    '''

    # Retrieve active connections
    pgActiveConn = pgUtils.dbQuery(
                        dbconn,
                        'SELECT count(*) FROM pg_stat_activity;'
                   )[0][0]

    # Retrieve max connections
    pgMaxConn = pgUtils.dbQuery(
                    dbconn,
                    'SHOW max_connections;'
                )[0][0]

    # Calculate percentage of active connections
    pgConnPerc = (int(pgActiveConn)*100.0)/int(pgMaxConn)

    if pgConnPerc > maxConnPerc:

        status = False

        if not os.path.exists('reports'):
            os.mkdir('reports')

        f = open(reportFile, 'a')
        f.write(
            'PostgreSQL active connections over threshold: {}% (max {}%)\n'
            .format(pgConnPerc, maxConnPerc)
        )
        f.close()
    else:
        status = True

    # Return formatted output
    return json.dumps({
        'pgConnPerc': "{0:.1f}".format(pgConnPerc),
        'passed': status
    })


def checkAll():
    '''
    Executes all checks
    '''
    dbNumOut = checkDBNum()
    dbDiskOut = checkDBDisk()
    dbConnOut = checkDBConn()

    return [dbNumOut, dbDiskOut, dbConnOut]


# MAIN

initVars()

# Initialize master connection to DB
dbconn = pgUtils.dbConnect()

# Execute checks based on 'mode'
result = {
    'dbnum': checkDBNum,
    'disk': checkDBDisk,
    'conn': checkDBConn,
    'all': checkAll
}[mode]()

# Close master connection to DB
dbconn.close()

# Print output
print(result)
